precision highp float;
uniform float time;
uniform vec2 resolution;

const float maxIter = 128.0;

float mandelbrot(vec2 uv) {
  vec2 c = 2.0 * uv; // - vec2(0.7, 0.0);
	c = c / abs(sin(time/7.0) * 300.0);
	c = c - vec2(0.65, 0.45);
  vec2 z = vec2(0.0);
  float iter = 0.0;

  for (float i = 0.0; i < maxIter; i++) {
    z = vec2(z.x * z.x - z.y * z.y,
             2.0 * z.x * z.y) + c;
        if (dot(z, z) > 4.0 ) return iter / maxIter;
        iter++;
  }
  return 0.0;
}

vec3 colorHash(float m) {
	float x = sin(m) * 25.0;
	float y = sin(m);
	float z = sin(m) * 15.0;
	return vec3(x/5.0, y/5.0, z/5.0);
}

void main( void ) {
  	vec2 uv = (gl_FragCoord.xy -
            0.5 * resolution.xy) / resolution.x;
	

  float m = mandelbrot(uv);
	
  //vec3 col = vec3(m);
	vec3 col = normalize(colorHash(m)+ vec3(m) * 0.5);

  gl_FragColor = vec4(col, 1.0) + (gl_FragCoord.x/ resolution.x);

}