#version 430
uniform vec3 inputColor;
uniform float time;
uniform vec2 resolution;
out vec4 fragColour;

const float MAX_ITER = 256.0;

float mandelbrot(vec2 uv) {
  //vec2 c = 0.00001 * uv - vec2(0.65, -0.45);
  vec2 c = 1.0/pow(2.0,time) * uv - vec2(0.66307, -0.43696);
  vec2 z = vec2(0.0);
  float iter = 0.0;
  for (;iter < MAX_ITER; iter++) {
    z = vec2(z.x * z.x - z.y * z.y,
             2.0 * z.x * z.y) + c;
    if(dot(z, z) > 4.0) return (iter) / MAX_ITER;
    
  }
  return 0.0;
}

void main(void) {
  vec2 uv = (gl_FragCoord.xy - 0.5 * resolution.xy) / resolution.y;
  //double a = 0.5;
  if (uv.x > -0.001 && uv.x < 0.001) {
    fragColour = vec4(1.0);
  } else if (uv.y > -0.001 && uv.y < 0.001) {
    fragColour = vec4(1.0);
  } else {

  vec3 col = vec3(0.0);
  float m = mandelbrot(uv);
  
  fragColour = vec4(m, m, m, 1.0);
  }
}

// void main() {
//   vec2 uv = (gl_FragCoord.xy - 0.5 * resolution.xy) / resolution.y;
//   double col = 0.1;
//   fragColour = vec4(cos(uv), 0.5+uv.x, 1.0);
// }
