#include "../imgui/imgui.h"
#include "../imgui/backends/imgui_impl_glfw.h"
#include "../imgui/backends/imgui_impl_opengl3.h"
#include <stdio.h>
#define GL_SILENCE_DEPRECATION
#if defined(IMGUI_IMPL_OPENGL_ES2)
#include <GLES2/gl2.h>
#endif
#include <GL/glew.h>
#define GLFW_DLL
#include <GLFW/glfw3.h>

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>

#define SHADERSET "basic"

static void glfw_error_callback(int error, const char* description)
{
    fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

const char *loadShaderFile(std::string name, int mode) {
  //* 0x1 vertex
  //* 0x2 teselation
  //* 0x4 geometry
  //* 0x8 fragment
  // std::ifstream File;
  FILE *File = NULL;
  
  if (mode & 0x01) { 
    // File = std::ifstream("./shaders/" + name + ".vs", std::ios_base::in);
    File = fopen64(("./shaders/" + name + ".vs").c_str(), "r");
  }
  if (mode & 0x02) { 
    // File = std::ifstream("./shaders/" + name + ".ts", std::ios_base::in);
    File = fopen64(("./shaders/" + name + ".ts").c_str(), "r");
  }
  if (mode & 0x04) { 
    // File = std::ifstream("./shaders/" + name + ".gs", std::ios_base::in);
    File = fopen64(("./shaders/" + name + ".gs").c_str(), "r");
  }
  if (mode & 0x08) { 
    // File = std::ifstream("./shaders/" + name + ".fs", std::ios_base::in);
    File = fopen64(("./shaders/" + name + ".fs").c_str(), "r");
  }

  // if (!File.is_open())
  //   return NULL;
  
  // std::string shader;
  // if (File) {
  //   std::stringstream ss;
  //   ss << File.rdbuf();
  //   shader = ss.str();
  // }
  
  // File.close();
  // return shader.c_str();
  
  if (!File)
    return NULL;
  
  fseeko64(File, 0, SEEK_END);
  long long int length = ftell(File);
  fseeko64(File, 0, SEEK_SET);
  char *output = (char *)malloc(length);
  fread(output, 1, length, File);
  fclose(File);
  return output;
}


// const char* vertex_shader =
// "#version 430\n"
// "in vec3 vp;"
// "void main() {"
// "  gl_Position = vec4(vp, 1.0);"
// "}";

// const char* fragment_shader =
// "#version 430\n"
// "\n"
// "uniform vec4 inputColour;\n"
// "out vec4 fragColour;\n"
// "\n"
// "void main() {\n"
// "  fragColour = vec4(0.2, 1.0, 0.2, 1.0);\n"
// "}\n";

float points1[] = {
   -1.0f,  1.0f,  0.0f,
   -1.0f, -1.0f,  0.0f,
    1.0f,  1.0f,  0.0f,
};

float colors[] = {
  1.0f, 0.0f,  0.0f,
  0.0f, 1.0f,  0.0f,
  0.0f, 0.0f,  1.0f
};

int main(int, char**)
{
    // Setup window
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return 1;

    const char* glsl_version = "#version 430";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Create window with graphics context
    GLFWwindow* window = glfwCreateWindow(1280, 720, "Shader sandbox", NULL, NULL);
    if (window == NULL) {
      glfwTerminate();
      return 1;
    }
        

    std::cout << "window created\n";

    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync
    glfwWindowHint(GLFW_SAMPLES, 4); // 4x AA

    glewExperimental = GL_TRUE;
    glewInit();
    glEnable(GL_DEPTH_TEST); // enable depth-testing
    glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"

    std::cout << "glew started\n";

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    ImGui::StyleColorsDark();

    std::cout << "imgui context created\n";

    // Setup Platform/Renderer backends
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    std::cout << "glfw for opengl " << glsl_version << " started\n";

    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

    auto t_start = std::chrono::high_resolution_clock::now();
    auto t_now = std::chrono::high_resolution_clock::now();
    float time = std::chrono::duration_cast<std::chrono::duration<float>>(t_now - t_start).count();

    //* openGL shader loading
        GLuint points_vbo = 0; // vertex buffer object
        glGenBuffers(1, &points_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, points_vbo);
        glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(float), points1, GL_STATIC_DRAW);
        //glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(float), points2, GL_DYNAMIC_DRAW);
        
        GLuint colors_vbo = 0;
        glGenBuffers(1, &colors_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, colors_vbo);
        glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(float), colors, GL_DYNAMIC_DRAW);

        GLuint vao = 0; // vertex array object
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, points_vbo);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        // GLuint vao = 0; // vertex array object
        // glGenVertexArrays(1, &vao);
        // glBindVertexArray(vao);
        // glBindBuffer(GL_ARRAY_BUFFER, points_vbo);
        // glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        // glBindBuffer(GL_ARRAY_BUFFER, colors_vbo);
        // glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        // glEnableVertexAttribArray(0);
        // glEnableVertexAttribArray(1);


        GLuint vs = glCreateShader(GL_VERTEX_SHADER);
        const char *vertex_shader = loadShaderFile(SHADERSET, 0x01);
        glShaderSource(vs, 1, &vertex_shader, NULL);
        glCompileShader(vs);
        GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
        const char *fragment_shader = loadShaderFile(SHADERSET, 0x08);
        glShaderSource(fs, 1, &fragment_shader, NULL);
        glCompileShader(fs);

        GLuint shader_program = glCreateProgram();
        glAttachShader(shader_program, fs);
        glAttachShader(shader_program, vs);

        // glBindAttribLocation(shader_programme, 0, "vertex_position");
        // glBindAttribLocation(shader_programme, 1, "vertex_color");

        glLinkProgram(shader_program);

        ImVec4 shader_col = ImVec4(0.0f, 0.0f, 0.00f, 1.00f);
        GLint shaderColV = glGetUniformLocation(shader_program, "inputColor");
        GLint shaderTime = glGetUniformLocation(shader_program, "time");
        GLint shaderResV= glGetUniformLocation(shader_program, "resolution");
        int window_height, window_width;
        

    while (!glfwWindowShouldClose(window)) {
      glfwPollEvents();
      ImGui_ImplOpenGL3_NewFrame();
      ImGui_ImplGlfw_NewFrame();
      ImGui::NewFrame();

        {

            t_now = std::chrono::high_resolution_clock::now();
            time = std::chrono::duration_cast<std::chrono::duration<float>>(t_now - t_start).count();
            glfwGetWindowSize(window, &window_width, &window_height);

            static float userZoom = 0.0f;
            static float userXcoor = 0.0f;
            static float userYcoor = 0.0f;

            ImGui::Begin("Settings");

            
            
            ImGui::SliderFloat("Zoom", &userZoom, 0.0f, 1.0f);
            ImGui::SliderFloat("X", &userXcoor, -1.0f, 1.0f);
            ImGui::SliderFloat("Y", &userYcoor, -1.0f, 1.0f);
            ImGui::ColorEdit3("clear color", (float*)&clear_color);
            ImGui::ColorEdit3("shader input", (float*)&shader_col);

            ImGui::Text("Time (s): %.2f", time);
            ImGui::Text("VSync enabled on %dx%d", window_width, window_height);
            ImGui::Text("Render average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
            ImGui::Text("Rendered on: %s", glGetString(GL_RENDERER));
            ImGui::Text("With gl version: %s", glGetString(GL_VERSION));
            ImGui::Text("To je vjedy...");
            
            ImGui::End();
        }
        // Rendering
        ImGui::Render();
        int display_w, display_h;
        glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        //! opengl render
          //* fill shader uniforms
          glUniform1f(shaderTime, time);
          glUniform3f(shaderColV, shader_col.x, shader_col.y, shader_col.z);

          glUniform2f(shaderResV, (float)window_width, (float)window_height);

          glUseProgram(shader_program);
          glBindVertexArray(vao);
          glDrawArrays(GL_TRIANGLES, 0, 6);
        
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        glfwSwapBuffers(window);

    }

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}